package com.zuitt.example;

public class Car {
    // [Access Modifier]
    //used to restrict the scope of a class constructor, variable, method or data member

    // Four types of Access Modifier
    // 1. Default (no keyword)
    // 2. Private
    // 4. Protected
    // 5. Public

    // Class Creation
    // Four parts of class creation

    // 1. Properties - characteristics of an object; also known as variable.

    private String name;
    private String brand;
    private int yearOfMake;

    // Makes additional component of a car; no need to import because this is a composition feature; Driver is a class wrapper
    private Driver driver;

    // 2. Constructor - is used to create/instantiate an object.

    // a. Empty Constructor - creates object that doesnt have any args/params. Also referred as default constructor.
    public Car() {
        // to set a default value upon instantiation
        this.yearOfMake = 2000;
        // Every time a new car is added, it will always have a driver named Alejandro
        this.driver = new Driver("Alejandro");
    };

    // b. Parametarized Constructor - creates an object with args/params.
    public Car(String name, String brand, int yearOfMake) {
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver("Alejandro");
    }

    // 3. Getters and Setters - get and set the values of each property of an object.

    // Getters - retrieves the value of instantiated object
    public String getName() {
        return this.name;
    }
    public String getBrand() {
        return this.brand;
    }
    public int getYearOfMake() {
        return this.yearOfMake;
    }

    public String getDriverName(){
        return this.driver.getName();
    }


    // Setters - used to change the default value of an instantiated object.
    public void  setName(String name) {
        this.name = name;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public void setYearOfMake(int yearOfMake) {
        // can also be modified to add validation
        if (yearOfMake <= 2022) {
            this.yearOfMake = yearOfMake;
        }
    }

    public void setDriver(String driver) {
        //this will invoke the setName() method of the Driver class
        this.driver.setName(driver);
    }

    // 4. Methods - functions that an object can perform (action). This is optional
    public void drive() {
        System.out.println("This car is running. Broom! Broom!");


    }
}
