package com.zuitt.example;
//Child class of Animal
// [Extends] keyword is used to inherit the properties and method of the parent class
public class Dog extends Animal{

    // Properties
    private String breed;

    // Constructor
    public Dog() {
        // super() - to have a direct access to the original constructor/ parent class.
        super();
        this.breed = "Retriever";
    }

    public Dog(String name, String color, String breed) {
        super(name, color);
        this.breed = breed;
    }

    public String getBreed() {
        return  this.breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    // method
    public void speak() {
        System.out.println("Woof! Woof!");
    }

    public void call() {
        super.call();
        System.out.println("Hi my name is " + this.name + ", I am a dog" );
    }
}
